#!/usr/bin/env python

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import time
import praw
import twitter
import inspect
import logging
import OAuth2Util
import sb_config as sb

# configure the logger
logging.basicConfig(filename='twitter_sidebar.log',
                    level=logging.INFO,
                    format='%(asctime)s %(message)s')

# User config
# --------------------------------------------------------------------
# Don't include the /r/
SUBREDDIT_NAME = ''

# These can be retrieved from you twitter app
CONSUMER_KEY = ''
CONSUMER_SECRET = ''
ACCESS_TOKEN = ''
ACCESS_TOKEN_SECRET = ''

# Twitter IDs are different than usernames, you can get them by going
# http://tweeterid.com/ with the @username of the account
TWITTER_IDS = []

# Number of tweets to fetch from each twitter account
NUM_TWEETS = 3

# --------------------------------------------------------------------


# import my developer settings
try:
    import bot
    SUBREDDIT_NAME = bot.SUBREDDIT_NAME
    CONSUMER_KEY = bot.CONSUMER_KEY
    CONSUMER_SECRET = bot.CONSUMER_SECRET
    ACCESS_TOKEN = bot.ACCESS_TOKEN
    ACCESS_TOKEN_SECRET = bot.ACCESS_TOKEN_SECRET
    TWITTER_IDS = bot.TWITTER_IDS
except ImportError:
    pass


# this function is formatting the dictionary of variables
# for logging in the event of an exception
def format_var_str(dic):
    s = ''
    for var, val in dic.items():
        s += ('\t' * 8) + '{}: {}\n'.format(var, val)

    return s


def get_tweets(api, user_id):
    statuses = api.GetUserTimeline(user_id)[:NUM_TWEETS]
    tweets = []
    for s in statuses:
        tweets.append([s.user.screen_name,
                       s.text, s.created_at_in_seconds,
                       s.GetId()])

    return tweets


def sort_tweets(tweets):
    return sorted(tweets, key=lambda time: time[2], reverse=True)


def update_sidebar(r, tweets):
    tw_str = ''
    for tweet in tweets:
        uname = tweet[0].encode('ascii', 'ignore')
        txt = tweet[1].encode('ascii', 'ignore')
        t_id = tweet[3]

        tw_str += sb.TWEET_TEMPLATE.format(uname=uname, tweet=txt, t_id=t_id)

    sb_str = sb.SB_TOP + tw_str + sb.SB_BOTTOM

    print('Updating sidebar...')
    sub = r.get_subreddit(SUBREDDIT_NAME)
    r.update_settings(sub, description=sb_str)


def main():
    r = praw.Reddit(user_agent='Tweets_In_Sidebar v2.0 /u/cutety')
    o = OAuth2Util.OAuth2Util(r, print_log=True)
    o.refresh(force=True)  # force it to refresh the token

    api = twitter.Api(consumer_key=CONSUMER_KEY,
                      consumer_secret=CONSUMER_SECRET,
                      access_token_key=ACCESS_TOKEN,
                      access_token_secret=ACCESS_TOKEN_SECRET)

    # Main loop
    while True:
        try:
            tweets = []
            print('Getting tweets...')
            for u_id in TWITTER_IDS:
                [tweets.append(t) for t in get_tweets(api, u_id)]
            tweets = sort_tweets(tweets)  # sort tweets by newest-oldest
            update_sidebar(r, tweets)
        except Exception, e:
            print('ERROR: {}'.format(e))

            # log the error event with specific information from the traceback
            traceback_log = '''
            ERROR: {e}
            File "{fname}", line {lineno}, in {fn}
            Time of error: {t}

            Variable dump:

            {g_vars}
            {l_vars}
            '''
            # grabs the traceback info
            frame, fname, lineno, fn = inspect.trace()[-1][:-2]
            # dump the variables and get formated strings
            g_vars = 'Globals:\n' + format_var_str(frame.f_globals)
            l_vars = 'Locals:\n' + format_var_str(frame.f_locals)

            logging.error(traceback_log.format(e=e, lineno=lineno, fn=fn,
                                               fname=fname,
                                               t=time.strftime('%c'),
                                               g_vars=g_vars, l_vars=l_vars))

        print('Sleeping...')
        time.sleep(900)  # go to sleep for 15 minutes


if __name__ == '__main__':
    if not SUBREDDIT_NAME:
        print('Subreddit name has not been set.\nExiting...')
        exit(1)

    main()
