# This is the config file for the sidebar because it can
# only be updated all at once, this will store the markdown
# used around the twitch streams section

SB_TOP = '''
Replace with top section of sidebar\n\n
'''

SB_BOTTOM = '''
Replace with bottom section of sidebar
'''

# this is how you will want each tweet to be formatted
# if you want to rearrange or change formatting, you must
# leave the {variable_name} as they are to have it the
# data to be put in correctly
TWEET_TEMPLATE = '''
\n[@{uname}](http://twitter.com/{uname})\n
[{tweet}](http://twitter.com/{uname}/status/{t_id})\n
'''
