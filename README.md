Post Tweets to Sidebar
================================

Installations
-------------

This script requires python 2, praw, praw-oauth2util, and python-twitter.

To install them type in your command line:

    pip install praw praw-oauth2util python-twitter

Reddit OAuth Setup
------------------

* [Go here on the account you want the bot to run on](https://www.reddit.com/prefs/apps/)
* Click on create a new app.
* Give it a name. 
* Select script from the radio buttons.
* Set the redirect uri to `http://127.0.0.1:65010/authorize_callback`
* After you create it you will be able to access the app key and secret.
* The **app key** is found here. Add this to the app_key variable in the oauth.ini file.

![here](http://i.imgur.com/7ybI5Fo.png) (**Do not give out to anybody**) 

* And the **app secret** here. Add this to the app_secret variable in the oauth.ini file.

![here](http://i.imgur.com/KkPE3EV.png) (**Do not give out to anybody**)

Note: On the first run of the bot you'll need to run it on a system that has a desktop environment.
So, if you're planning on running the bot on a server or VPS somewhere, run it on your computer
first. The first run will require you to authenticate it with the associated Reddit account by
opening a web page that looks like this:

![authentication page](http://i.imgur.com/se53uTq.png)

It will list different things it's allowed to do depending on the bots scope. After you
authenticate it, that page won't pop up again unless you change the OAuth credentials. And you'll
be free to run it on whatever environment you choose.

Twitter Authentication Setup
----------------------------

Go to [apps.twitter.com](http://apps.twitter.com) and create a new app.

Fill out the form on the next page:

![twitter app form](http://i.imgur.com/9q1NO8s.png)

Then you'll be able to retrieve your keys and access tokens from the
keys and access tokens tab.

Your consumer keys will look like this:

![consumer keys](http://i.imgur.com/S3tGixg.png)

Add those to the twitter_sidebar.py

Then go to the bottom of the page and find the button to generate your access
tokens.

After generating your access tokens you'll see something like this:

![access tokens](http://i.imgur.com/kDuMrAk.png)

Add those to the twitter_sidebar.py

Then find the account you want to pull tweets from, and retrieve their twitter ID,
and add that to the script.

Config
------
Set the SUBREDDIT_NAME variable to the name of the subreddit, don't include the /r/.

Add the twitter keys as described above to the corresponding variables.

Add the Twitter IDs to the TWITTER_IDS list `['123', '456']`. **Note: these are not Twitter Usernames, they are IDs**. Go to [tweeterid.com](http://tweeterid.com/) to get the ID from the username.

Set NUM_TWEETS to the number of tweets you want fetched from each twitter account.

In the sb_config file, set the SB_TOP to the text you want to appear before the tweets in the sidebar. And the SB_BOTTOM to be the text you want to appear after the tweets. You'll have to format it like you would with markdown. Instead of hitting enter twice, you can just type `\n\n` if to add two newlines (you'll want to end SB_TOP with `\n\n` to have a space before the tweets.

The TWEET_TEMPLATE is how each streamer will get formatted. You can change the formatting if you wish, just don't remove the `{variable_name}` or change the variable name inside the curly braces.

You can style them with the css in styles.css to achieve this look:

![styles](http://i.imgur.com/KD4Dhlm.png)

License
-------

The MIT License (MIT)

Copyright (c) 2015 Nick Hurst

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
